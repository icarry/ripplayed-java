package com.icarie.ripplayed.Controllers;

import com.icarie.ripplayed.*;

/**
 * Created by ROMMM on 7/18/15.
 */
public class ControllerFactory {

    public static AController ControllerBuilder(String name) {
        switch (name) {
            case  "GoogleMap.fxml":
                return new GoogleMapController();
            case  "ProfilPage.fxml":
                return new ProfilePageController();
            case  "WebBrowser.fxml":
                return new WebBrowserController();
            case "LandingPage.fxml":
                return new LandingPageController();
            case  "LoginPage.fxml":
                return new LoginController();
            default:
                return null;
        }
    }
}
