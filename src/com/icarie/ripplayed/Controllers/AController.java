package com.icarie.ripplayed.Controllers;

import javafx.scene.layout.VBox;

/**
 * Created by ROMMM on 7/18/15.
 */
public abstract class AController extends VBox {

    protected boolean isInit = false;

    public boolean isInit() {
        return isInit;
    }

    public void setIsInit(boolean isInit) {
        this.isInit = isInit;
    }

    public abstract void onLogout();

    public abstract void onSuccessfulLogin();

    public abstract void onSelectedController();
}
