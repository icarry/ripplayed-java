package com.icarie.ripplayed;

import com.icarie.ripplayed.Controllers.AController;
import com.icarie.ripplayed.Controllers.ControllerFactory;
import com.teamdev.jxbrowser.chromium.LoggerProvider;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    private final String URL = "LandingPage.fxml";
    private final String ICON = "ressources/map.png";
    private final String TITLE = "Ripplayed BETA";

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.getIcons().add(new Image(ICON));
        FXMLLoader loader = new FXMLLoader(getClass().getResource(URL));
        AController controller = ControllerFactory.ControllerBuilder(URL);
        loader.setController(controller);
        loader.setRoot(controller);
        LoggerProvider.setLevel(Level.OFF);
        Parent root;
        try {
            root = loader.load();
            primaryStage.setTitle(TITLE);
            primaryStage.setScene(new Scene(root, 900, 650));
            primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(AController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
