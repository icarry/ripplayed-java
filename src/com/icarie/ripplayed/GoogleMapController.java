package com.icarie.ripplayed;

import com.icarie.ripplayed.Controllers.AController;
import com.icarie.ripplayed.Networking.*;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import netscape.javascript.JSObject;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class GoogleMapController extends AController implements Initializable {

    @FXML private WebView webViewGoogleMap;
    private WebEngine gMapWebEngine;
    private JSObject jsobj;
    private Timeline fiveSecondsWonder;

    private Bridge bridge = new Bridge();

    public GoogleMapController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        gMapWebEngine = webViewGoogleMap.getEngine();
        gMapWebEngine.load(getClass().getResource("googleMap.html").toString());
        jsobj = (JSObject) gMapWebEngine.executeScript("window");
        jsobj.setMember("java", bridge);

        LandingPageController._instance.onSuccessfulLogin();
        GoogleMapController.this.setIsInit(true);
        displayMarkersOnMap();

        fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(5), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                displayMarkersOnMap();
            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
    }

    private void displayMarkersOnMap() {

        jsobj.call("deleteMarkers");

        new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getBroadcastListUrl(),
                null, new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {
                jsobj.call("displayMarkers", string);
            }

            @Override
            public void onServerFailedResponse(String string) {
                    System.out.println(string);
                }
        }).run();
    }

    private void setUserModeToListener() {

        new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.setUserMode(RipplayedWebServices.LISTENER), null, new OnServerResponseListener() {
            @Override

            public void onServerSuccessResponse(String string) {
                DataSingleton.getUser().setIsBroadcasting(false);
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }
        }).run();
    }

    @Override
    public void onSelectedController() {
        if (fiveSecondsWonder != null && fiveSecondsWonder.getStatus() == Animation.Status.PAUSED)
        fiveSecondsWonder.play();
    }

    @Override
    public void onLogout() {
        if (fiveSecondsWonder != null && fiveSecondsWonder.getStatus() == Animation.Status.RUNNING)
            fiveSecondsWonder.stop();
    }

    @Override
    public void onSuccessfulLogin() {
    }

    public class Bridge {

        public void getUserPos(Double lat, Double longitude) {
        }

        public void startListeningMode(String date, String songUrl, int userId) {

            setUserModeToListener();

            new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getTimeOfServer(), null, new OnServerResponseListener() {
                @Override
                public void onServerSuccessResponse(String string) {
                    try {
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                        Date date1 = format.parse(date);
                        Date serverDate = JsonParser.getServerTime(string);
                        long differenceInSec = (serverDate.getTime() - date1.getTime()) / 1000;

                        LandingPageController._instance.onHeaderButtonClicked("WebBrowser.fxml");
                        WebBrowserController._instance.onJoiningBroadcaster(differenceInSec, songUrl, userId);
                        fiveSecondsWonder.pause();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onServerFailedResponse(String string) {
                    System.out.println("Failed : GetTime: " + string);
                }

            }).run();
        }
    }
}
