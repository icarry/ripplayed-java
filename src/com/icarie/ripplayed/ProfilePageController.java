package com.icarie.ripplayed;

import com.icarie.ripplayed.Controllers.AController;
import com.icarie.ripplayed.Models.User;
import com.icarie.ripplayed.Networking.ApiRequest;
import com.icarie.ripplayed.Networking.DataSingleton;
import com.icarie.ripplayed.Networking.OnServerResponseListener;
import com.icarie.ripplayed.Networking.RipplayedWebServices;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by ROMMM on 3/21/15.
 */
public class ProfilePageController extends AController implements Initializable{

    @FXML VBox profilpage;

    @FXML Label labelFirstName;
    @FXML Label labelLastName;
    @FXML Label labelUserName;
    @FXML Label labelEmail;

    @FXML TextField textFieldFirstName;
    @FXML TextField textFieldLastName;
    @FXML TextField textFieldUserName;
    @FXML TextField textFieldEmail;
    @FXML Label labelLoginMethod;
    @FXML ImageView ImageViewProfil;
    @FXML Button buttonLogOut;

    private ResourceBundle lngBnd;

    User user = DataSingleton.getInstance().getUser();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            lngBnd = ResourceBundle
                    .getBundle("bundles.Bundle", new Locale(user.getLanguage().toLowerCase(), user.getLanguage().toUpperCase()));
        } catch (Exception e) {
            lngBnd = ResourceBundle
            .getBundle("bundles.Bundle", new Locale("fr", "FR"));
        }

        buttonLogOut.setText(lngBnd.getString("logout"));
        labelFirstName.setText(lngBnd.getString("firstName"));
        labelLastName.setText(lngBnd.getString("lastName"));
        labelEmail.setText(lngBnd.getString("email"));
        labelUserName.setText(lngBnd.getString("userName"));

        textFieldFirstName.setText(user.getFirstName());
        textFieldLastName.setText(user.getLastName());
        textFieldUserName.setText(user.getNickName());
        textFieldUserName.setText(user.getNickName());
        textFieldEmail.setText(user.getEmail());
        ImageViewProfil.setImage(new Image(user.getPictureUrl()));

        if (user.getAccountType() == "Facebook"){
            labelLoginMethod.setText(lngBnd.getString("loggedIn_via") + " Facebook");
        } else {
            labelLoginMethod.setText(lngBnd.getString("loggedIn_via") + " Google");
        }

        buttonLogOut.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                setUserModeToListener();

            }
        });
    }

    private void setUserModeToListener() {

        new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.setUserMode(RipplayedWebServices.LISTENER), null, new OnServerResponseListener() {
            @Override

            public void onServerSuccessResponse(String string) {
                DataSingleton.getUser().setIsBroadcasting(false);

                new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.getLogoutUrl(),
                        null, new OnServerResponseListener() {
                    @Override
                    public void onServerSuccessResponse(String string) {
                        DataSingleton.getPersistentCookieStore().removeAll();
                        LandingPageController._instance.onLogout();
                        LoginController._instance.onLogout();
                    }

                    @Override
                    public void onServerFailedResponse(String string) {
                        System.out.println(string);
                    }

                }).run();
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }
        }).run();
    }

    @Override
    public void onSelectedController() {

    }

    @Override
    public void onLogout() {

    }

    @Override
    public void onSuccessfulLogin() {

    }
}