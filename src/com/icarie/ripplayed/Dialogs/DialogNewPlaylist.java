package com.icarie.ripplayed.Dialogs;

import com.icarie.ripplayed.Networking.OnUserApprovalListener;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;

/**
 * Created by ROMMM on 5/29/15.
 */
public class DialogNewPlaylist {

    public static void createNewPlaylist(OnUserApprovalListener listener) {

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("New Playlist");
        dialog.setHeaderText("What name do you wish for your new playlist ?");
//        dialog.setContentText("Name");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> listener.onPlaylistCreation(name));
    }
}
