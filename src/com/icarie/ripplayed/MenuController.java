package com.icarie.ripplayed;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by ROMMM on 3/18/15.
 */
public class MenuController extends AnchorPane implements Initializable {

    @FXML
    private MenuItem menuItemQuit;

    public void setListeners() {

        menuItemQuit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setListeners();
    }
}
