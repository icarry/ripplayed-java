package com.icarie.ripplayed;

import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import java.net.URL;
import java.util.ResourceBundle;

public class TopHeaderController extends HBox implements Initializable {

    private boolean buttonsVisible = true;
    private ImageView imageViews[] = new ImageView[3];
    private String titles[] = new String[] {
            "ressources/map.png",
            "ressources/broadcast.png",
            "ressources/profile.png",
    };

    public void setButtonsVisible(boolean bool) {
        for (ImageView button : imageViews) {
            button.setVisible(bool);
        }
        buttonsVisible = bool;
    }

    public TopHeaderController() {

        this.setAlignment(Pos.CENTER);
        this.setPrefHeight(64);
        this.setPrefWidth(800);
        this.setPadding(new Insets(15, 12, 15, 12));
        this.setSpacing(20);
        this.setStyle("-fx-background-color: #336699;");
        HBox.setHgrow(this, Priority.ALWAYS);

        for (int i = 0; i < titles.length; i++) {
            imageViews[i] = new ImageView(new Image(titles[i]));
        }

        this.getChildren().addAll(imageViews);
        setListeners();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private void setListeners() {
        imageViews[0].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            LandingPageController._instance.onHeaderButtonClicked("GoogleMap.fxml");
            event.consume();
        });

        imageViews[1].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            LandingPageController._instance.onHeaderButtonClicked("WebBrowser.fxml");
            event.consume();
        });

        imageViews[2].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            LandingPageController._instance.onHeaderButtonClicked("ProfilPage.fxml");
            event.consume();
        });
    }

    public boolean AreButtonsVisible() {
        return buttonsVisible;
    }

    public interface OnHeaderButtonClickedListener {
        void onHeaderButtonClicked(String Url);
    }

    public interface OnJoiningBroadcasterListener {
        void onJoiningBroadcaster(long millisecond, String Url, int userID);
    }
}
