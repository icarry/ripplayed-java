package com.icarie.ripplayed;

import com.icarie.ripplayed.Controllers.AController;
import com.icarie.ripplayed.Controllers.ControllerFactory;
import com.icarie.ripplayed.Models.YTPlaylist;
import com.icarie.ripplayed.Networking.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class LandingPageController extends AController implements Initializable, TopHeaderController.OnHeaderButtonClickedListener {

    @FXML VBox borderPaneInclude;
    @FXML TopHeaderController topHeaderContent;

    private final String WEBBROWSER = "WebBrowser.fxml";
    private final String LOGINPAGE = "LoginPage.fxml";
    private final String LOGIN = "Login";

    private HashMap<String, FXMLLoader> loaders = new HashMap<>();
    private HashMap<String, Parent> panels = new HashMap<>();

    private boolean WebBrowserRan = false;

    public static LandingPageController _instance;

    public LandingPageController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        _instance = this;
        loadSubScene(LOGINPAGE);
        topHeaderContent.setButtonsVisible(false);
    }

    private void loadSubScene(String url) {
        try {
            FXMLLoader loader;
            if ((loader = loaders.get(url)) == null) {
                loader = new FXMLLoader(getClass().getResource(url));
                AController controller = ControllerFactory.ControllerBuilder(url);
                loader.setController(controller);
                loader.setRoot(controller);
                loaders.put(url, loader);
                panels.put(url, (Parent)loader.load());
            }
            AController controller = loader.getController();
            if (controller.isInit())
                controller.onSelectedController();
            borderPaneInclude.getChildren().add(panels.get(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onHeaderButtonClicked(String Url) {
        borderPaneInclude.getChildren().clear();
        borderPaneInclude.getChildren().removeAll();
        if (!topHeaderContent.AreButtonsVisible() && !Url.contains(LOGIN)) {
            topHeaderContent.setButtonsVisible(true);
        }
        loadSubScene(Url);
    }

    @Override
    public void onSuccessfulLogin() {
        this.topHeaderContent.setVisible(true);

        requestUserPlaylists(DataSingleton.getAuthObject().getToken_access());
    }

    @Override
    public void onLogout() {
        this.topHeaderContent.setVisible(false);

        for (Map.Entry<String, FXMLLoader> entry : loaders.entrySet()) {
            ((AController)entry.getValue().getController()).onLogout();
        }

        this.onHeaderButtonClicked(LOGINPAGE);
    }

    @Override
    public void onSelectedController() { }

    private void requestUserPlaylists(String tokenAccess) {

        new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getYTUserPlaylists(tokenAccess), null
                , new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {
                try {
                    ArrayList<YTPlaylist> playlists = JsonParser.getYoutubePlaylistIds(string);

                    DataSingleton.setYtPlaylists(playlists);

                    for (YTPlaylist playlist : playlists) {
                        retrievePlaylistItems(playlist, tokenAccess);
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }
        }).run();
    }

    private void retrievePlaylistItems(YTPlaylist playlist, String tokenAccess) {

        new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getYTUserPlaylistItems(playlist.getId(), tokenAccess), null
                , new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {
                JsonParser.getYoutubePlaylistsItems(playlist, string);

                if (WebBrowserRan == false) {
                    WebBrowserRan = true;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                //Preloading Webbrowser to be more efficient
                                FXMLLoader loader = new FXMLLoader(getClass().getResource(WEBBROWSER));
                                AController controller = ControllerFactory.ControllerBuilder(WEBBROWSER);
                                loader.setController(controller);
                                loader.setRoot(controller);
                                loaders.put(WEBBROWSER, loader);
                                panels.put(WEBBROWSER, (Parent) loader.load());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }
        }).run();
    }
}
