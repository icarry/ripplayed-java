package com.icarie.ripplayed.Models;

import java.net.URI;

/**
 * Created by ROMMM on 7/15/15.
 */
public class PersitantCookie {

    private String name;
    private String value;
    private URI uri;
    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }
}
