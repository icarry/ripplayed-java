package com.icarie.ripplayed.Models;

/**
 * Created by ROMMM on 5/25/15.
 */
public class YTItem {

    private String etag;
    private String name;
    private String id;
    private String description;
    private String YT_ID;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getYT_ID() {
        return YT_ID;
    }

    public void setYT_ID(String YT_ID) {
        this.YT_ID = YT_ID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    @Override
    public String toString() {
        return getName();
    }
}
