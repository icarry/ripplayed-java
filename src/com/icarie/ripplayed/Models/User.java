package com.icarie.ripplayed.Models;

/**
 * Created by ROMMM on 3/17/15.
 */
public class User {

    public static final String FACEBOOK = "facebook";
    public static final String GOOGLE = "google";

    private String firstName;
    private String lastName;
    private String nickName;
    private String pictureUrl;
    private String email;

    private String socialTokenAccess;
    private String accountType;

    private int ripplayed_id;
    private String ripplayed_token;
    private boolean isBroadcasting = false;
    private String country;
    private String language;

    public boolean isBroadcasting() {
        return isBroadcasting;
    }

    public void setIsBroadcasting(boolean isBroadcasting) {
        this.isBroadcasting = isBroadcasting;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSocialTokenAccess() {
        return socialTokenAccess;
    }

    public void setSocialTokenAccess(String socialTokenAccess) {
        this.socialTokenAccess = socialTokenAccess;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public int getRipplayed_id() {
        return ripplayed_id;
    }

    public void setRipplayed_id(int ripplayed_id) {
        this.ripplayed_id = ripplayed_id;
    }

    public String getRipplayed_token() {
        return ripplayed_token;
    }

    public void setRipplayed_token(String ripplayed_token) {
        this.ripplayed_token = ripplayed_token;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public User() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }
}
