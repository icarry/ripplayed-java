package com.icarie.ripplayed.Models;

import java.util.ArrayList;

/**
 * Created by ROMMM on 5/25/15.
 */
public class YTPlaylist {

    private String id;
    private String name;
    private String etag;
    private String description;
    private ArrayList<YTItem> items;

    public ArrayList<YTItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<YTItem> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getName() +  "//"+ getItems().size();
    }
}
