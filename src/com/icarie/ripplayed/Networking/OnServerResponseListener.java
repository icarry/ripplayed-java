package com.icarie.ripplayed.Networking;

/**
 * Created by ROMMM on 4/10/15.
 */
public interface OnServerResponseListener {
        public void onServerSuccessResponse(String string);
        public void onServerFailedResponse(String string);
}
