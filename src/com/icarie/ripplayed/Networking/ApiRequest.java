package com.icarie.ripplayed.Networking;

import netscape.javascript.JSObject;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by ROMMM on 4/10/15.
 */
public class ApiRequest extends Thread {

    private RequestType type;
    private String requestUrl;
    private Map<String, String> params;
    private OnServerResponseListener listener;

    public enum RequestType {
        GET,
        POST,
        DELETE,
        PUT
    }

    public ApiRequest(RequestType type, String url, Map<String, String> params, OnServerResponseListener listener) {
        this.type = type;
        this.requestUrl = url;
        this.params = params;
        this.listener = listener;
    }

    @Override
    public void run() {

        String response;
        try {
            switch (type) {
                case GET:
                    response = GetRequest.sendGet(requestUrl);
                    break;
                case POST:
                    response = PostRequest.sendPost(requestUrl, params);
                    break;
                case DELETE:
                    response = DeleteRequest.sendDelete(requestUrl);
                    break;
                case PUT:
                    response = PutRequest.sendPut(requestUrl, params);
                    break;
                default:
                    response = "Invalid Request Type";
                    break;
            }

            // checking if response is a valid JSON object
            if (JsonParser.isJSONValid(response))
                listener.onServerSuccessResponse(response);
            else
                listener.onServerFailedResponse(response);

        } catch (Exception e) {
            e.printStackTrace();
            listener.onServerFailedResponse(e.getMessage());
        }
    }
}
