package com.icarie.ripplayed.Networking;

import com.icarie.ripplayed.Models.Broadcaster;
import com.icarie.ripplayed.Models.GoogleAuthObject;
import com.icarie.ripplayed.Models.User;
import com.icarie.ripplayed.Models.YTPlaylist;
import com.icarie.ripplayed.Utils.PersistentCookieStore;

import java.util.ArrayList;

/**
 * Created by ROMMM on 5/22/15.
 */
public class DataSingleton {

    private static DataSingleton ourInstance = new DataSingleton();
    public static User user;
    public static GoogleAuthObject authObject;
    public static ArrayList<YTPlaylist> ytPlaylists;
    public static PersistentCookieStore persistentCookieStore = new PersistentCookieStore();
    public static Broadcaster currentBroadcaster;

    public static PersistentCookieStore getPersistentCookieStore() {
        return persistentCookieStore;
    }

    public static void setPersistentCookieStore(PersistentCookieStore persistentCookieStore) {
        DataSingleton.persistentCookieStore = persistentCookieStore;
    }

    public static ArrayList<YTPlaylist> getYtPlaylists() {
        return ourInstance.ytPlaylists;
    }

    public static void setYtPlaylists(ArrayList<YTPlaylist> ytPlaylists) {
        DataSingleton.ourInstance.ytPlaylists = ytPlaylists;
    }

    public static GoogleAuthObject getAuthObject() {
        return getInstance().authObject;
    }

    public static void setAuthObject(GoogleAuthObject authObject) {
        DataSingleton.getInstance().authObject = authObject;
    }

    public static DataSingleton getInstance() {
        return ourInstance;
    }

    public static User getUser() {
        return getInstance().user;
    }

    public static void setUser(User user) {
        DataSingleton.ourInstance.user = user;
    }

    private DataSingleton() {
        user = new User();
        authObject = new GoogleAuthObject();
    }

    public static Broadcaster getCurrentBroadcaster() {
        return currentBroadcaster;
    }

    public static void setCurrentBroadcaster(Broadcaster broadcaster) {
        DataSingleton.getInstance().currentBroadcaster = broadcaster;
    }
}
