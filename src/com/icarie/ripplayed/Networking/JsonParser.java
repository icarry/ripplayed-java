package com.icarie.ripplayed.Networking;

import com.icarie.ripplayed.Models.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ROMMM on 5/23/15.
 */
public class JsonParser {

    // RIPPLAYED
    private final static String USERNAME = "username";
    private final static String DATE = "date";
    private final static String SONG_URL = "song_url";
    private final static String LONGITUDE = "longitude";
    private final static String LATITUDE = "latitude";
    private final static String ALBUM_COVER = "album_cover";
    private final static String ALBUM_NAME = "album_name";
    private final static String ARTIST_NAME = "artist_name";
    private final static String SONG_TITLE = "song_title";
    private final static String RIPPLAYED_ID = "ripplayed_id";
    private final static String RIPPLAYED_TOKEN = "ripplayed_token";
    private final static String TOKEN = "token";
    private final static String TIME = "time";

    // YOUTUBE
    private final static String ITEMS = "items";
    private final static String ETAG = "etag";
    private final static String ID = "id";
    private final static String TITLE = "title";
    private final static String DESCRIPTION = "description";
    private final static String SNIPPET = "snippet";
    private final static String VIDEOID = "videoId";
    private final static String RESSOUCE_ID = "resourceId";
    private final static String THUMBNAILS = "thumbnails";
    private final static String DEFAULT = "default";
    private final static String URL = "url";

    // GOOGLE PLUS
    private final static String PICTURE = "picture";
    private final static String EMAIL = "email";
    private final static String FIRSTNAME = "given_name";
    private final static String LASTNAME = "family_name";
    private final static String NAME = "name";

    // GOOGLE
    private final static String ACCESS_TOKEN= "access_token";
    private final static String ID_TOKEN = "id_token";
    private final static String REFRESH_TOKEN = "refresh_token";

    public static GoogleAuthObject parseGoogleAuth(String json) {

        try {
            GoogleAuthObject googleAuthObject = new GoogleAuthObject();

            JSONObject jsonObject = new JSONObject(json);
            googleAuthObject.setToken_access(jsonObject.getString(ACCESS_TOKEN));
            googleAuthObject.setId_token(jsonObject.getString(ID_TOKEN));
            googleAuthObject.setRefresh_token(jsonObject.getString(REFRESH_TOKEN));

            return googleAuthObject;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static ArrayList<YTPlaylist> getYoutubePlaylistIds(String json) {
        ArrayList<YTPlaylist> playlists = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(ITEMS);

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                YTPlaylist ytPlaylist = new YTPlaylist();
                ytPlaylist.setEtag(jsonObject.getString(ETAG));
                ytPlaylist.setId(jsonObject.getString(ID));

                JSONObject snippet = jsonObject.getJSONObject(SNIPPET);
                ytPlaylist.setName(snippet.getString(TITLE));
                ytPlaylist.setDescription(snippet.getString(DESCRIPTION));

                playlists.add(ytPlaylist);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return playlists;
        }
        return playlists;
    }

    public static void getYoutubePlaylistsItems(YTPlaylist playlist, String json) {
        ArrayList<YTItem> items = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(ITEMS);

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                YTItem item = new YTItem();
                item.setEtag(jsonObject.getString(ETAG));
                item.setId(jsonObject.getString(ID));

                JSONObject snippet = jsonObject.getJSONObject(SNIPPET);
                item.setName(snippet.getString(TITLE));
                item.setDescription(snippet.getString(DESCRIPTION));

                JSONObject thumbnails = snippet.getJSONObject(THUMBNAILS);
                thumbnails = thumbnails.getJSONObject(DEFAULT);
                item.setUrl(thumbnails.getString(URL));

                JSONObject videoID = snippet.getJSONObject(RESSOUCE_ID);
                item.setYT_ID(videoID.getString(VIDEOID));

                items.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        playlist.setItems(items);
    }

    public static ArrayList<YTItem> getYoutubeSearchItems(String json) {
        ArrayList<YTItem> items = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(ITEMS);

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                YTItem item = new YTItem();
                item.setEtag(jsonObject.getString(ETAG));

                JSONObject idStuff = jsonObject.getJSONObject(ID);
                item.setId(idStuff.getString(VIDEOID));
                item.setYT_ID(idStuff.getString(VIDEOID));

                JSONObject snippet = jsonObject.getJSONObject(SNIPPET);
                item.setName(snippet.getString(TITLE));
                item.setDescription(snippet.getString(DESCRIPTION));

                JSONObject thumbnails = snippet.getJSONObject(THUMBNAILS);
                thumbnails = thumbnails.getJSONObject(DEFAULT);
                item.setUrl(thumbnails.getString(URL));

                items.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }

    public static void parseGoogleUserData(String json, String token_access) {
        try {
            User user = new User();
            JSONObject jsonObject = new JSONObject(json);

            user.setPictureUrl(jsonObject.getString(PICTURE));
            user.setNickName(jsonObject.getString(NAME));
            user.setFirstName(jsonObject.getString(FIRSTNAME));
            user.setLastName(jsonObject.getString(LASTNAME));
            user.setEmail(jsonObject.getString(EMAIL));
            user.setSocialTokenAccess(token_access);
            user.setAccountType(User.GOOGLE);

            user.setCountry(System.getProperty("user.country"));
            user.setLanguage(System.getProperty("user.language"));

            DataSingleton.setUser(user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static boolean parseRipplayedInfo(String string) {

        User user = DataSingleton.getUser();

        try {
            JSONObject jsonObject = new JSONObject(string);
            user.setRipplayed_id(jsonObject.getInt(RIPPLAYED_ID));
            if (jsonObject.has(RIPPLAYED_TOKEN))
                user.setRipplayed_token(jsonObject.getString(RIPPLAYED_TOKEN));
            else
                user.setRipplayed_token(jsonObject.getString(TOKEN));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ArrayList<Broadcaster> getBroadcasters(String json) {

        ArrayList<Broadcaster> broadcasters = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(json);
            for(int i = 0; i < jsonArray.length(); i++) {

                Broadcaster broadcaster = new Broadcaster();

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                broadcaster.setLatitude(jsonObject.getInt("latitude"));
                broadcaster.setLongitude(jsonObject.getInt("longitude"));
                broadcaster.setSongUrl(jsonObject.getString("song_url"));
                broadcaster.setUsername(jsonObject.getString("username"));
                broadcaster.setAlbum_cover(jsonObject.getString("album_cover"));
                broadcaster.setArtist_name(jsonObject.getString("artist_name"));
                broadcaster.setSong_title(jsonObject.getString("song_title"));
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                broadcaster.setDate(format.parse(jsonObject.getString(DATE)));
                broadcaster.setAlbum_name(jsonObject.getString("album_name"));
                broadcaster.setId(jsonObject.getInt("id"));

                broadcasters.add(broadcaster);
            }

            return broadcasters;
        } catch (JSONException e) {
            e.printStackTrace();
            return broadcasters;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getServerTime(String string) {
        try {
            JSONObject jsonObject = new JSONObject(string);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            return format.parse(jsonObject.getString(TIME));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Broadcaster getCurrentBroadcaster(String string) {
        try {
            Broadcaster broadcaster = new Broadcaster();
            JSONObject jsonObject = new JSONObject(string);
            broadcaster.setId(jsonObject.getInt(ID));
            broadcaster.setSongUrl(jsonObject.getString(SONG_URL));
            broadcaster.setSong_title(jsonObject.getString(SONG_TITLE));
            broadcaster.setAlbum_name(jsonObject.getString(ALBUM_NAME));
            broadcaster.setAlbum_cover(jsonObject.getString(ALBUM_COVER));
            broadcaster.setUsername(jsonObject.getString(USERNAME));
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            broadcaster.setDate(format.parse(jsonObject.getString(DATE)));
            broadcaster.setLongitude(jsonObject.getInt(LONGITUDE));
            broadcaster.setLatitude(jsonObject.getInt(LATITUDE));
            return broadcaster;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
