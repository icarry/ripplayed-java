/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icarie.ripplayed.Networking;

import com.icarie.ripplayed.Models.User;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author ROMMM
 */
public class GetRequest{

	private static StringBuffer response;

	// HTTP GET request
	public static String sendGet(final String url) throws Exception {

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);

		// add request header
		User user = DataSingleton.getUser();
		if(user.getRipplayed_token() != null && url.contains("ripplayed")) {
			String userCredentials = user.getRipplayed_id() + ":" + user.getRipplayed_token();
			String basicAuth = "Basic " + new String(new Base64().encode(userCredentials.getBytes()));
			request.addHeader ("Authorization", basicAuth);
		}

		HttpResponse response = client.execute(request);

		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " +
				response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		System.out.println(result.toString());
		return result.toString();

//		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//
//		User user = DataSingleton.getUser();
//		if(user.getRipplayed_token() != null) {
//			String userCredentials = user.getRipplayed_id() + ":" + user.getRipplayed_token();
//			String basicAuth = "Basic " + new String(new Base64().encode(userCredentials.getBytes()));
//			con.setRequestProperty ("Authorization", basicAuth);
//		}
//
//		con.setRequestProperty("Content-Type","application/x-www- form-urlencoded");
//		con.setDoOutput(true);
//		con.setDoInput(true);
//
//		int responseCode = con.getResponseCode();
//		System.out.println("\nSending 'GET' request to URL : " + url);
//		System.out.println("Response Code : " + responseCode);
//
//		BufferedReader in = new BufferedReader(
//				new InputStreamReader(con.getInputStream()));
//		String inputLine;
//		response = new StringBuffer();
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//
//		System.out.println(response.toString());
//		return (response.toString());
	}
}