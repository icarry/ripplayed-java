package com.icarie.ripplayed.Networking;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ROMMM on 4/10/15.
 */
public class DeleteRequest {

    public static String sendDelete(String URL) throws Exception{
        URL url = new URL(URL);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty(
                "Content-Type", "application/x-www-form-urlencoded");
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();
        return "Success";
    }
}
