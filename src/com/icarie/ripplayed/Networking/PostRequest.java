/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icarie.ripplayed.Networking;

import com.icarie.ripplayed.Models.User;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ROMMM
 */
public class PostRequest {

	private static StringBuffer response;

	public static String sendPost(final String url, Map<String, String> params) throws Exception {

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		if (!url.contains("ripplayed")) {
			post.addHeader("Content-Type", "application/x-www-form-urlencoded");

			if (params != null) {
				List<NameValuePair> urlParameters = new ArrayList<>();

				for (Map.Entry<String, String> entry : params.entrySet()) {
					urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
				post.setEntity(new UrlEncodedFormEntity(urlParameters));
			}
		} else {
			post.addHeader("Content-Type", "application/json");
			post.addHeader("accept", "application/json");

			User user = DataSingleton.getUser();
			String userCredentials = user.getRipplayed_id() + ":" + user.getRipplayed_token();
			String basicAuth = "Basic " + new String(new Base64().encode(userCredentials.getBytes()));
			post.addHeader ("Authorization", basicAuth);

			if (params != null) {
				JSONObject jsonObject = new JSONObject(params);
				post.setEntity(new StringEntity(jsonObject.toString()));
			}
		}

		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " +
				response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		System.out.println(result.toString());
		return result.toString();
	}
}

//	// HTTP POST request
//	public static String sendPost(final String url, Map<String, String> params) throws Exception {
//
//		HttpClient client = new DefaultHttpClient();
//		HttpPost post = new HttpPost(url);
//
//		URL obj = new URL(url);
//
//		HttpsURLConnection con;
//		if (url.contains("https"))
//			con = (HttpsURLConnection) obj.openConnection();
//
//		String toWrite = "";
//		for (Map.Entry<String, String> entry : params.entrySet()) {
//			if (toWrite.length() != 0)
//				toWrite += "&";
//			toWrite += entry.getKey()+ "=" + entry.getValue();
//			System.out.println(entry.getKey() + " ::::: " + entry.getValue());
//		}
//
//		byte[] postData = toWrite.getBytes(StandardCharsets.UTF_8);
//		int postDataLength = postData.length;
//
//		//add request header
//		con.setRequestMethod("POST");
//		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//		con.setRequestProperty("charset", "utf-8");
//		con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
//		con.setUseCaches(false);
//		con.setDoOutput(true);
//		con.setDoInput(true);
//
//		// Send post request
//		try {
//			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//			wr.write(postData);
//			wr.flush();
//			wr.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		int responseCode = con.getResponseCode();
////		System.out.println("\nSending 'POST' request to URL : " + url);
////		System.out.println("Response Code : " + responseCode);
//
//		BufferedReader in = new BufferedReader(
//		        new InputStreamReader(con.getInputStream()));
//		String inputLine;
//		response = new StringBuffer();
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//
//		return (response.toString());
//	}
//}