package com.icarie.ripplayed.Networking;

/**
 * Created by ROMMM on 5/21/15.
 */
public class RipplayedWebServices {

    public static final String DEVICE_ID = "desktop_app";

    // RIPPLAYED API
    public static final String BASE_URL = "http://api.ripplayed.com/";
    public static final String AUTH = "auth/";
    public static final String ANONYMOUS = "anonymous";
    public static final String SOCIAL = "social";
    public static final String LOGOUT = "logout";

    public static final String BROADCAST = "broadcast/";
    public static final String LIST = "list";
    public static final String TIME = "time";
    public static final String USER = "user/";
    public static final String SETMODE = "setmode/";
    public static final String SETPOS = "setpos";
    public static final String START_BROADCASTING = "play_song";

    // Youtube API
    public static final String BASE_YT_URL = "https://www.googleapis.com/youtube/v3/";
    public static final String PLAYLIST_ITEMS = "playlistItems?playlistId=";
    public static final String RETRIVE_YT_PLAYLIST = "playlists/";
    public static final String YT_PART = "&part=id,snippet";
    public static final String YT_MINE = "&mine=true&access_token=";
    public static final String QUERY = "q=";
    public static final String SEARCH = "search?";
    public static final String TYPE = "&type=video";
    public static final String VIDEO_ID = "&videoId=";
    public static final String MAX_RESULT = "&maxResults=5";
    public static final String BASE_OAUTH = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=";
    public static final String GOOGLE = "google";
    public static final String LISTENER = "listener";
    public static final String BROADCASTER = "broadcaster";

    public static String getGoogleUserInfo(String token_access) {
        return BASE_OAUTH +  token_access;
    }

    public static String getYoutubeSearch(String query, String token_access) {
        query = query.replace(" ", "%20");
        return BASE_YT_URL + SEARCH + QUERY + query + YT_PART + TYPE + MAX_RESULT + YT_MINE + token_access;
    }

    public static String getAnonymousAuthUrl() {
        return BASE_URL + AUTH + ANONYMOUS;
    }

    public static String getSocialAuthUrl() {
        return BASE_URL + AUTH + SOCIAL;
    }

    public static String getLogOutUrl() {
        return BASE_URL + AUTH + LOGOUT;
    }

    public static String getBroadcastListUrl() {
        return BASE_URL + BROADCAST + LIST;
    }

    public static String getYTUserPlaylistItems(String playlistID, String tokenAccess) {
        return BASE_YT_URL + PLAYLIST_ITEMS + playlistID + YT_PART + YT_MINE + tokenAccess;
    }

    public static String getYTUserPlaylists(String tokenAccess) {
        return BASE_YT_URL + RETRIVE_YT_PLAYLIST + "?" + YT_PART + YT_MINE + tokenAccess;
    }

    public static String getTimeOfServer() {
        return BASE_URL + BROADCAST + TIME;
    }

    public static String setUserMode(String mode) {
        return BASE_URL + USER + SETMODE + mode;
    }

    public static String getUrlStartBroadcasting() {
        return BASE_URL + BROADCAST + START_BROADCASTING;
    }

    public static String getBroadcasterInfo(int id) {
        return BASE_URL + BROADCAST + id;
    }

    public static String getUrlSetPosition() {
        return BASE_URL + USER + SETPOS;
    }

    public static String getLogoutUrl() {
        return BASE_URL + AUTH + LOGOUT;
    }
}
