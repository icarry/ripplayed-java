package com.icarie.ripplayed;

import com.icarie.ripplayed.Controllers.AController;
import com.icarie.ripplayed.Models.Broadcaster;
import com.icarie.ripplayed.Models.YTItem;
import com.icarie.ripplayed.Models.YTPlaylist;
import com.icarie.ripplayed.Networking.*;
import com.icarie.ripplayed.Utils.MusicQueue;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.events.*;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import netscape.javascript.JSObject;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by ROMMM on 4/9/15.
 */
public class WebBrowserController extends AController implements Initializable, OnUserApprovalListener, TopHeaderController.OnJoiningBroadcasterListener {

    private static final String ADD_TO_QUEUE = "Add to queue";
    private static final String ADDED_TO_QUEUE = "Remove from queue";

    private final Image mute = new Image("ressources/mute.png");
    private final Image unmute = new Image("ressources/unmute.png");

    private MusicQueue musicQueue = new MusicQueue();
    private JSObject jsobj;
    private PlayerBridge bridge = new PlayerBridge();

    @FXML
    private TabPane tabPaneMenu;
    private Tab tabYoutube = new Tab("Youtube");
    private Tab tabRipplayed = new Tab("Ripplayed");
    private Accordion accordionYTPlaylist = new Accordion();

    @FXML
    private AnchorPane anchorPaneSearch;
    @FXML
    private StackPane paneYoutubeWebview;
    @FXML
    private WebView webViewYoutubePlayer;

    @FXML
    private ImageView buttonPrev;
    @FXML
    private ImageView buttonNext;
    @FXML
    private ImageView buttonMute;
    @FXML
    private Slider sliderVolume;
    @FXML
    private VBox vBoxBroadcaster;

    @FXML
    private ImageView imageViewSongCover;
    @FXML
    private Label textBroadcasterName;
    @FXML
    private Label textSongTitle;
    @FXML
    private Label textArtistName;

    private ArrayList<YTPlaylist> playlists;

    private Browser browser = new Browser();
    private final BrowserView browserView = new BrowserView(browser);
    private Timeline task;

    private boolean isMuted = false;
    private String current_song = "";
    public static WebBrowserController _instance;

    private long latitude;
    private long longitude;

    public WebBrowserController() {

        new ApiRequest(ApiRequest.RequestType.GET, "http://ip-api.com/json", null, new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {
                try {
                    JSONObject jsonObject = new JSONObject(string);
                    latitude = jsonObject.getLong("lat");
                    longitude = jsonObject.getLong("lon");
                } catch (Exception e) {

                }
            }

            @Override
            public void onServerFailedResponse(String string) {

            }
        }).run();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        browserView.getBrowser().loadURL(getClass().getResource("YoutubePlayer.html").toString());

        browser.addLoadListener(new LoadListener() {
            @Override
            public void onStartLoadingFrame(StartLoadingEvent startLoadingEvent) {

            }

            @Override
            public void onProvisionalLoadingFrame(ProvisionalLoadingEvent provisionalLoadingEvent) {

            }

            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent finishLoadingEvent) {
                sliderVolume.setValue(50);
                browser.executeJavaScript("setVolume(50)");
            }

            @Override
            public void onFailLoadingFrame(FailLoadingEvent failLoadingEvent) {

            }

            @Override
            public void onDocumentLoadedInFrame(FrameLoadEvent frameLoadEvent) {

            }

            @Override
            public void onDocumentLoadedInMainFrame(LoadEvent loadEvent) {

            }
        });
        
        paneYoutubeWebview.getChildren().add(browserView);

            _instance = this;

            buttonPrev.setImage(new Image("ressources/previous.png"));
            buttonNext.setImage(new Image("ressources/forward.png"));
            buttonMute.setImage(mute);


//            jsobj = (JSObject)browserView.executeJavaScript("window");
//            jsobj.setMember("java", bridge);

            setListeners();

            setIsInit(true);

            displaySearchBar();

            DataSingleton.getYtPlaylists().forEach(this::displayPlaylist);

            tabYoutube.setContent(accordionYTPlaylist);
            tabPaneMenu.getTabs().addAll(tabYoutube, tabRipplayed);
    }

    public Node addPlaylistToPane(String query, ArrayList<YTItem> itemsFound) {

        VBox mainBox = new VBox();
        mainBox.setPadding(new Insets(5, 5, 5, 5));
        for (int j = 0; j < itemsFound.size(); j++) {
            VBox itemBox = new VBox();
            final YTItem item = itemsFound.get(j);
            ImageView imageItem = new ImageView(item.getUrl());
            imageItem.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 0);");
            Label name = new Label(item.getName());
            name.setAlignment(Pos.CENTER);
            Button buttonAddToQueue = new Button(ADD_TO_QUEUE);
            buttonAddToQueue.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (buttonAddToQueue.getText().contains(ADD_TO_QUEUE)) {
                        buttonAddToQueue.setText(ADDED_TO_QUEUE);
                        musicQueue.addToQueue(item);
                        if (musicQueue.getCurrentPlaylistSize() == 0)
                            browserView.getBrowser().executeJavaScript("playVideoById(\"" + item.getYT_ID() + "\");");
                    } else {
                        buttonAddToQueue.setText(ADD_TO_QUEUE);
                        musicQueue.removeFromQueue(item);
                    }
                }
            });
            itemBox.getChildren().addAll(name, imageItem, buttonAddToQueue);
            itemBox.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    if (event.getButton() == MouseButton.SECONDARY) {

                    } else if (event.getButton() == MouseButton.PRIMARY) {

                        HashMap<String, String> longlat = new HashMap<String, String>();
                        longlat.put("longitude", longitude+"");
                        longlat.put("latitude", latitude+"");

                        new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.getUrlSetPosition(), longlat, new OnServerResponseListener() {
                            @Override
                            public void onServerSuccessResponse(String string) {

                                new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.setUserMode(RipplayedWebServices.BROADCASTER), null, new OnServerResponseListener() {
                                    @Override
                                    public void onServerSuccessResponse(String string) {
                                        musicQueue.addToQueue(item);
//                                musicQueue.setCurrentSong(item);
                                        browserView.getBrowser().executeJavaScript("playVideoById(\"" + item.getYT_ID() + "\");");

                                        HashMap<String, String> params = new HashMap<String, String>();
                                        params.put("song_url", "https://www.youtube.com/watch?v=" + item.getYT_ID());
                                        params.put("song_name", item.getName());
                                        params.put("album_name", item.getName());
                                        params.put("album_cover", item.getUrl());
                                        params.put("artist_name", item.getName());
                                        params.put("genre", item.getName());

                                        DataSingleton.getUser().setIsBroadcasting(true);

                                        new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.getUrlStartBroadcasting(), params, new OnServerResponseListener() {
                                            @Override
                                            public void onServerSuccessResponse(String string) {
                                                loadBroadcasterInfo(item);
                                            }

                                            @Override
                                            public void onServerFailedResponse(String string) {
                                                System.out.println(string);
                                            }

                                        }).run();
                                    }

                                    @Override
                                    public void onServerFailedResponse(String string) {
                                        System.out.println(string);
                                    }

                                }).run();
                            }

                            @Override
                            public void onServerFailedResponse(String string) {
                                System.out.println(string);
                            }
                        }).run();

                        event.consume();
                    }
                }
            });
            mainBox.getChildren().add(itemBox);
        }
        return mainBox;
    }

    private void displaySearchBar() {

        HBox hBox = new HBox();
        hBox.setMargin(hBox, new Insets(5, 0, 10, 0));
        hBox.setPadding(new Insets(5, 5, 5, 5));
        hBox.setHgrow(hBox, Priority.ALWAYS);
        TextField textField = new TextField();
        textField.setPromptText("Search...");
        textField.setAlignment(Pos.CENTER_LEFT);
        Button buttonSearch = new Button("Search");
        buttonSearch.setAlignment(Pos.CENTER_RIGHT);
        buttonSearch.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getYoutubeSearch(textField.getText(), DataSingleton.getAuthObject().getToken_access()), null
                        , new OnServerResponseListener() {
                    @Override
                    public void onServerSuccessResponse(String string) {
                        try {
                            ArrayList<YTItem> itemsFound = JsonParser.getYoutubeSearchItems(string);

                            Tab searchTab = new Tab(textField.getText());
                            searchTab.setContent(addPlaylistToPane(textField.getText(), itemsFound));
                            tabPaneMenu.getTabs().add(searchTab);

                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onServerFailedResponse(String string) {
                        System.out.println(string);
                    }
                }).run();
                event.consume();
            }
        });

        hBox.getChildren().addAll(textField, buttonSearch);
        anchorPaneSearch.getChildren().add(hBox);
    }

    private void displayPlaylist(YTPlaylist playlist) {

        TitledPane titledPane = new TitledPane(playlist.getName(), addPlaylistToPane(playlist.getName(), playlist.getItems()));
        accordionYTPlaylist.getPanes().add(titledPane);
    }

//    private static void enableFirebug(final WebEngine engine) {
//        engine.executeScript("if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
//    }

    private void setListeners() {

        buttonMute.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (isMuted) {
                    isMuted = false;
                    browserView.getBrowser().executeJavaScript("unMute()");
                    buttonMute.setImage(unmute);
                } else {
                    isMuted = true;
                    browserView.getBrowser().executeJavaScript("mute()");
                    buttonMute.setImage(mute);
                }
            }
        });
        buttonNext.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (musicQueue.getCurrentPlaylistSize() > 0)
                    browserView.getBrowser().executeJavaScript("playVideoById(\"" + musicQueue.getNextSong() + "\");");
                event.consume();
            }
        });

        buttonPrev.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (musicQueue.getNumberOfPlayedSong() > 0)
                    browserView.getBrowser().executeJavaScript("playVideoById(\"" + musicQueue.getPreviousSong() + "\");");
                event.consume();
            }
        });

        sliderVolume.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                                Number old_val, Number new_val) {
                try {
                    browserView.getBrowser().executeJavaScript("setVolume(" + (int) new_val.doubleValue() + ")");
                    if (isMuted) {
                        isMuted = false;
                        browserView.getBrowser().executeJavaScript("unMute()");
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    public void onPlaylistCreation(String name) {
        TitledPane newPlaylist = new TitledPane(name, null);
        accordionYTPlaylist.getPanes().add(newPlaylist);
    }

    @Override
    public void onJoiningBroadcaster(long second, String Url, int userID) {

        String url[] = Url.split("=");
        current_song = url[1];

        browserView.getBrowser().executeJavaScript("playVideoById(\"" + current_song + "\");");
        browserView.getBrowser().executeJavaScript("seek(" + (second + 1) + ");");

        if (task != null)
            task.stop();

        task = new Timeline(new KeyFrame(Duration.seconds(5), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getBroadcasterInfo(userID), null, new OnServerResponseListener() {
                    @Override
                    public void onServerSuccessResponse(String string) {
                        try {
                            Broadcaster broadcaster = JsonParser.getCurrentBroadcaster(string);

                            if (broadcaster == null) {
                                //TODO Determine what to do if broadcaster is null
                            } else {
                                DataSingleton.setCurrentBroadcaster(broadcaster);
                                String url[] = broadcaster.getSongUrl().split("=");
                                if (!current_song.equals(url[1])) {
                                    loadBroadcasterInfo(broadcaster);
                                    current_song = url[1];
                                    browserView.getBrowser().executeJavaScript("playVideoById(\"" + current_song + "\");");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onServerFailedResponse(String string) {
                        System.out.println(string);
                    }
                }).run();
            }
        }));
        task.setCycleCount(Timeline.INDEFINITE);
        task.play();
    }

    private void loadBroadcasterInfo(Broadcaster broadcaster) {

        imageViewSongCover.setImage(new Image(broadcaster.getAlbum_cover()));
        textBroadcasterName.setText(broadcaster.getUsername());
        textSongTitle.setText(broadcaster.getSong_title());
        textArtistName.setText(broadcaster.getArtist_name());
    }

    private void loadBroadcasterInfo(YTItem item) {

        imageViewSongCover.setImage(new Image(item.getUrl()));
        textBroadcasterName.setText(DataSingleton.getUser().getNickName());
        textSongTitle.setText(item.getName());
        textArtistName.setText(item.getDescription());
    }

    @Override
    public void onSelectedController() {
    }

    @Override
    public void onLogout() {
        browser.loadURL("YoutubePlayer.html");
    }

    @Override
    public void onSuccessfulLogin() {

    }

    public class PlayerBridge {

        public void onMusicCompletion() {
            browserView.getBrowser().executeJavaScript("playVideoById(\"" + musicQueue.getNextSong() + "\");");
        }
    }
}