package com.icarie.ripplayed;

import com.icarie.ripplayed.Controllers.AController;
import com.icarie.ripplayed.Models.GoogleAuthObject;
import com.icarie.ripplayed.Networking.*;
import com.icarie.ripplayed.Utils.PersistentCookieStore;
import com.icarie.ripplayed.Utils.RedirectFinder;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class LoginController extends AController implements Initializable {

    public enum AUTH_TYPE {
        UNAUTH,
        GOOGLE,
        FACEBOOK
    }

    @FXML private ImageView buttonLoginFB;
    @FXML private ImageView buttonLoginGoogle;
    @FXML private WebView webViewLoginPage;

    private static final String APP_ID = "436574223160893";
    private static final String PERMISSIONS = "public_profile";
    private static String access_token;
    private AUTH_TYPE auth_type = AUTH_TYPE.UNAUTH;
    private PersistentCookieStore persistentCookieStore;
    public static LoginController _instance;

    private final static String googleAuth = "https://accounts.google.com/o/oauth2/auth?scope=email%20profile%20https://www.googleapis.com/auth/youtube&redirect_uri=http://localhost&response_type=code&client_id=269905511411-m2kt1upui8bjk6j8g55ccdm3fdd6f1o2.apps.googleusercontent.com";

    public LoginController() { }

    private void setListeners() {

        webViewLoginPage.getEngine().locationProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldLocation, String newLocation) {
                System.out.println(newLocation);

                String token_identifier;

                if (auth_type == AUTH_TYPE.FACEBOOK) {
                    token_identifier = "access_token=";
                    if (newLocation.contains("access_token=")) {
                        access_token = newLocation.substring(newLocation.lastIndexOf(token_identifier) + token_identifier.length(), newLocation.lastIndexOf('&'));
                        String expires_in = newLocation.substring(newLocation.lastIndexOf('=') + 1);

                        FacebookClient facebookClient = new DefaultFacebookClient(access_token);
                        User user = facebookClient.fetchObject("me", User.class);

                        com.icarie.ripplayed.Models.User userToStore = new com.icarie.ripplayed.Models.User();
                        userToStore.setPictureUrl(RedirectFinder.getDirectUrl("http://graph.facebook.com/" + user.getId() + "/picture?type=large"));
                        if (user.getUsername() == null)
                            userToStore.setNickName(user.getFirstName() + "." + user.getLastName());
                        else
                            userToStore.setNickName(user.getUsername());
                        userToStore.setFirstName(user.getFirstName());
                        userToStore.setLastName(user.getLastName());
                        userToStore.setEmail(user.getEmail());
                        userToStore.setSocialTokenAccess(access_token);
                        userToStore.setAccountType(com.icarie.ripplayed.Models.User.FACEBOOK);
                        userToStore.setCountry(System.getProperty("user.country"));
                        userToStore.setLanguage(System.getProperty("user.language"));

                        DataSingleton.setUser(userToStore);

                        getLoggedOntoRipplayed();

//                        LandingPageController._instance.onHeaderButtonClicked("GoogleMap.fxml");
                    }
                } else if (auth_type == AUTH_TYPE.GOOGLE) {
                    token_identifier = "http://localhost/?code=";
                    if (newLocation.contains(token_identifier)) {
                        String code_identifier = newLocation.substring(newLocation.lastIndexOf(token_identifier) + token_identifier.length());

                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("redirect_uri", "http://localhost");
                        params.put("code", code_identifier);
                        params.put("client_id", "269905511411-m2kt1upui8bjk6j8g55ccdm3fdd6f1o2.apps.googleusercontent.com");
                        params.put("client_secret", "ExoGM68VvWGoW6U5QSajTEQf");
                        params.put("grant_type", "authorization_code");

                        new ApiRequest(ApiRequest.RequestType.POST, "https://accounts.google.com/o/oauth2/token", params, new OnServerResponseListener() {
                            @Override
                            public void onServerSuccessResponse(String string) {
                                System.out.println("SUCCESS Google" + string);

                                GoogleAuthObject googleAuthObject = JsonParser.parseGoogleAuth(string);
                                DataSingleton.setAuthObject(googleAuthObject);

                                requestUserInfo(googleAuthObject);
                            }

                            @Override
                            public void onServerFailedResponse(String string) {
                                System.out.println(string);
                            }

                        }).run();
                    }
                }
            }
        });

        buttonLoginFB.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                loadFbAuth();
                event.consume();
            }
        });

        buttonLoginGoogle.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                loadGoogleAuth();
                event.consume();
            }
        });
    }

    private void loadFbAuth() {
        auth_type = AUTH_TYPE.FACEBOOK;
        webViewLoginPage.getEngine().load("https://www.facebook.com/dialog/oauth?"
                + "client_id=" + APP_ID
                + "&redirect_uri=https://www.facebook.com/connect/login_success.Html"
                + "&scope=" + PERMISSIONS
                + "&response_type=token");
    }

    private void loadGoogleAuth() {
        auth_type = AUTH_TYPE.GOOGLE;
        webViewLoginPage.getEngine().load(googleAuth);
    }

    private void requestUserInfo(GoogleAuthObject googleAuthObject) {
        new ApiRequest(ApiRequest.RequestType.GET, RipplayedWebServices.getGoogleUserInfo(googleAuthObject.getToken_access()),
                null, new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {

                JsonParser.parseGoogleUserData(string, googleAuthObject.getToken_access());


                getLoggedOntoRipplayed();
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }

        }).run();
    }

    private void getLoggedOntoRipplayed() {

        com.icarie.ripplayed.Models.User user = DataSingleton.getUser();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("account_type", user.getAccountType());
        params.put("access_token", user.getSocialTokenAccess());
        params.put("device_id", RipplayedWebServices.DEVICE_ID);

        new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.getSocialAuthUrl(),
                params, new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {

                if (JsonParser.parseRipplayedInfo(string)) {
                    LandingPageController._instance.onHeaderButtonClicked("GoogleMap.fxml");
                }
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }

        }).run();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        _instance = this;

        buttonLoginFB.setImage(new Image("ressources/facebook.png"));
        buttonLoginGoogle.setImage(new Image("ressources/google.png"));

        loginOptionsLoading();
        setListeners();
    }

    private void loginOptionsLoading() {
        persistentCookieStore = DataSingleton.getPersistentCookieStore();

        if (persistentCookieStore.cookiesContains("google"))
            loadGoogleAuth();
        else if (persistentCookieStore.cookiesContains("facebook"))
            loadFbAuth();
        else
            webViewLoginPage.getEngine().load(getClass().getResource("LoginBackground.html").toString());
    }

    @Override
    public void onSelectedController() {

    }

    @Override
    public void onLogout() {
        loginOptionsLoading();
    }

    @Override
    public void onSuccessfulLogin() {

    }
}
