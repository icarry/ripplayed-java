package com.icarie.ripplayed.Utils;

import com.icarie.ripplayed.Networking.ApiRequest;
import com.icarie.ripplayed.Networking.OnServerResponseListener;
import com.icarie.ripplayed.Networking.RipplayedWebServices;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PersistentCookieStore implements CookieStore, Runnable {

    private CookieStore store;
    private CookieManager manager;
    public final static String PATH = "ck.dat";
    public final static String DELIMITOR = ";";

    public PersistentCookieStore() {

        // get the default in memory cookie store
        manager = new CookieManager();
        manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        store = manager.getCookieStore();

        // and add them store
        try {
            List<String> cookiesParsed = ReadFile.readFile(PATH);
            for (String cookie : cookiesParsed) {
                String[] parts = cookie.split(DELIMITOR);

                Map<String, List<String>> headers = new LinkedHashMap<String, List<String>>();
                headers.put("Set-Cookie", Arrays.asList(parts[0] + "=" + parts[1]));
                java.net.CookieHandler.getDefault().put(new URI(parts[2]), headers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // add a shutdown hook to write out the in memory cookies
        Runtime.getRuntime().addShutdownHook(new Thread(this));
    }

    public void run() {

        List<HttpCookie> cookies = store.getCookies();
        List<URI> uris = store.getURIs();

        if (cookies != null && uris != null && cookies.size() > 0 && uris.size() > 0) {
            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(PATH, false)))) {
                for (int i = 0; i < cookies.size(); i++) {
                    HttpCookie cookie = cookies.get(i);
                    for (URI uri : uris) {
                        if (uri.toString().contains(cookie.getDomain())) {
                            out.println(cookie.getName() + DELIMITOR + cookie.getValue() + DELIMITOR + uri + DELIMITOR + cookie.getDomain());
                            break;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        new ApiRequest(ApiRequest.RequestType.POST, RipplayedWebServices.setUserMode(RipplayedWebServices.LISTENER), null, new OnServerResponseListener() {
            @Override
            public void onServerSuccessResponse(String string) {
                System.out.println(string);
            }

            @Override
            public void onServerFailedResponse(String string) {
                System.out.println(string);
            }
        }).run();
    }

    public boolean cookiesContains(String query) {
        for (HttpCookie cookie : getCookies()) {
            if (cookie.getDomain().contains(query))
                return true;
        }
        return false;
    }

    public void	add(URI uri, HttpCookie cookie) {
        store.add(uri, cookie);
    }

    public List<HttpCookie> get(URI uri) {
        return store.get(uri);
    }

    public List<HttpCookie> getCookies() {
        return store.getCookies();
    }

    public List<URI> getURIs() {
        return store.getURIs();
    }

    public boolean remove(URI uri, HttpCookie cookie) {
        return store.remove(uri, cookie);
    }

    public boolean removeAll()  {
        store.removeAll();
        return deleteCookiesFile();
    }

    public boolean deleteCookiesFile() {
        return new File(PATH).delete();
    }
}
