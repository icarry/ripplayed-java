package com.icarie.ripplayed.Utils;

import com.icarie.ripplayed.Models.YTItem;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ROMMM on 8/25/2015.
 */
public class MusicQueue {

    private ArrayList<YTItem> currentPlaylist = new ArrayList<>();
    private ArrayList<YTItem> playedSongs = new ArrayList<>();
    private boolean random = false;
    private int current_index = 0;
    private YTItem currentSong;

    public void addToQueue(YTItem item) {
        currentPlaylist.add(item);
    }

    public void removeFromQueue(YTItem item) {
        currentPlaylist.remove(item);
    }

    public void setRandom(boolean value) {
        random = value;
    }

    public boolean getRandom() {
        return random;
    }

    public String getNextSong() {
        YTItem item;
        if (random == true) {
            Random rand = new Random();
            item = currentPlaylist.get(rand.nextInt(currentPlaylist.size() + 1));
        } else {
            if (current_index > currentPlaylist.size()) {
                current_index = 0;
                item = currentPlaylist.get(current_index);
            } else {
                item = currentPlaylist.get(current_index);
                current_index++;
            }
        }
        playedSongs.add(currentSong);
        currentSong = item;
        return item.getYT_ID();
    }

    public String getPreviousSong() {
        try {
            current_index--;
            if (current_index < 0)
                current_index = 0;
            return playedSongs.get(--current_index).getYT_ID();
        } catch (Exception e) {
            return "";
        }
    }

    public void setCurrentSong(YTItem currentSong) {
        this.currentSong = currentSong;
    }

    public int getNumberOfPlayedSong() {
        return playedSongs.size();
    }

    public int getCurrentPlaylistSize() {
        return currentPlaylist.size();
    }
}
