package com.icarie.ripplayed.Utils;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ROMMM on 8/28/2015.
 */
public class RedirectFinder {

    public static String getDirectUrl(String link) {
        String resultUrl = link;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(link).openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                String locationUrl = connection.getHeaderField("Location");

                if (locationUrl != null && locationUrl.trim().length() > 0) {
                    resultUrl = getDirectUrl(locationUrl);
                }
            }
        } catch (Exception e) {
        }

        return resultUrl;
    }
}
